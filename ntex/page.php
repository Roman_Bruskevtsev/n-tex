<?php
/**
 *
 * @package WordPress
 * @subpackage N-tex
 * @since 1.0
 * @version 1.0
 */

get_header(); 

get_template_part( 'template-parts/page/content', 'title' );

if( have_rows('content') ):

	while ( have_rows('content') ) : the_row();

		if( get_row_layout() == 'categories_section' ) :
			get_template_part( 'template-parts/page/content', 'categories_section' );
		elseif( get_row_layout() == 'clients_section' ) :
			get_template_part( 'template-parts/page/content', 'clients_section' );
		elseif( get_row_layout() == 'testimonials_section' ) :
			get_template_part( 'template-parts/page/content', 'testimonials_section' );
		elseif( get_row_layout() == 'services_section' ) :
			get_template_part( 'template-parts/page/content', 'services_section' );
		elseif( get_row_layout() == 'gallery_section' ) :
			get_template_part( 'template-parts/page/content', 'gallery_section' );
		endif;

	endwhile;

endif;

if ( have_posts() ) {
	while ( have_posts() ) { the_post(); ?>
		<section class="page__content" data-aos="fade-up" data-aos-duration="600">
			<div class="container">
				<div class="row">
					<div class="col">
						<?php the_content(); ?>
					</div>
				</div>
			</div>
		</section>
	<?php }
}


get_footer();