'use strict';

class GeneralClass{

    constructor(){
        this.init();
    }

    init(){
        this.documentReady();
        this.windowLoad();
        this.closePopup();
        this.orderProduct();
        this.getCall();
        this.readMore();
        this.readLess();
    }

    documentReady(){
        jQuery(document).ready(function($) {
            if($('.feedback__slider').length){
                $('.feedback__slider').slick({
                    infinite:       true,
                    autoplay:       true,
                    autoplaySpeed:  10000,
                    speed:          1200,
                    arrows:         false,
                    dots:           true
                });
            }
            if($('.clients__slider').length){
                $('.clients__slider').slick({
                    infinite:       true,
                    autoplay:       true,
                    autoplaySpeed:  1000,
                    speed:          1000,
                    arrows:         false,
                    dots:           false,
                    slidesToShow:   12,
                    slidesToScroll: 1,
                    responsive: [
                        {
                            breakpoint: 1280,
                            settings: {
                                slidesToShow: 8
                            }
                        },
                        {
                            breakpoint: 992,
                            settings: {
                                slidesToShow: 6
                            }
                        },
                        {
                            breakpoint: 768,
                            settings: {
                                slidesToShow: 3
                            }
                        },
                        {
                            breakpoint: 480,
                            settings: {
                                slidesToShow: 2
                            }
                        }
                    ]
                });
            }

            if($('.products__row').length){
                let $grid = $('.products__row'),
                    currentId = '*';
                $grid.isotope({
                    itemSelector: '.product__cell'
                });


                $('.tags li').on('click', function(){
                    let id =  '.' + $(this).data('id');
                    $('.tags li').removeClass('active');
                    $(this).addClass('active');

                    if(currentId == $(this).data('id') ){
                        $grid.isotope({ filter: '*' });
                        $('.tags li').removeClass('active');
                        currentId = '*';

                    } else {
                        $grid.isotope({ filter: id })
                        currentId = $(this).data('id');
                    }
                });
            }

            $('.gallery__section').lightGallery({
                selector: '.image',
                thumbnail: false,
                download: false
            }); 

            $('.products__row').lightGallery({
                selector: '.thumbnail',
                thumbnail: false,
                download: false
            }); 

            /*Mobile menu*/
            $('.mobile__btn').on('click', function(){
                $(this).toggleClass('show');
                $('.mobile__menu').toggleClass('show');
            });
        });    
    }

    windowLoad(){
        jQuery(document).ready(function($) {
            ntex.googleMap();
            AOS.init();
        });
    }

    closePopup(){
        let closeBtns = document.querySelectorAll('.popup .close, .popup__wrapper');

        if( closeBtns ){
            let popups = document.querySelectorAll('.popup');
            closeBtns.forEach(function(closeBtn){
                closeBtn.addEventListener('click', function(){
                    document.querySelector('.popup__wrapper').classList.remove('show');
                    popups.forEach((popup) => {
                        popup.classList.remove('show');
                    });
                });
            });
        }
    }

    orderProduct(){
        let orders = document.querySelectorAll('.product button');

        if( orders ){
            orders.forEach(function(order){
                order.addEventListener('click', function(){
                    let popup = document.querySelector('.popup.order'),
                        popupWrapper = document.querySelector('.popup__wrapper'),
                        productName = this.closest('.product').querySelector('.text h3').textContent,
                        fieldProductName = popup.querySelector('input[name="product-name"]');

                    fieldProductName.value = productName;

                    popupWrapper.classList.add('show');
                    popup.classList.add('show');
                });
            });
        }
    }

    getCall(){
        let orders = document.querySelectorAll('.nav__bar button, .mobile__menu button, .call__btn');

        if( orders ){
            orders.forEach(function(order){
                order.addEventListener('click', function(){
                    let popup = document.querySelector('.popup.contact'),
                        popupWrapper = document.querySelector('.popup__wrapper');

                    popupWrapper.classList.add('show');
                    popup.classList.add('show');
                });
            });
        }
    }

    readMore(){
        let readButtons = document.querySelectorAll('.service .show__more');

        if( readButtons ){
            readButtons.forEach(function(readButton){
                readButton.addEventListener('click', function(){
                    let block = this.closest('.service');

                    block.classList.add('show');
                    
                });
            });
        }
    }

    readLess(){
        let readButtons = document.querySelectorAll('.service .show__less');

        if( readButtons ){
            readButtons.forEach(function(readButton){
                readButton.addEventListener('click', function(){
                    let block = this.closest('.service');

                    block.classList.remove('show');
                    
                });
            });
        }
    }

    googleMap(){
        let map = document.querySelector('.map');
        if( map ){
            let address = map.dataset.str,
                latitude = map.dataset.latitude,
                longitude = map.dataset.longitude,
                zoom = parseInt(map.dataset.zoom),
                markerIcon = map.dataset.marker,
                style = [],
                myLatlng = new google.maps.LatLng(latitude, longitude),
                styledMap = new google.maps.StyledMapType(style, { name: "Styled Map" }),
                mapOptions = {
                    zoom: zoom,
                    disableDefaultUI: false,
                    scrollwheel: false,
                    zoomControl: true,
                    streetViewControl: true,
                    center: myLatlng
                },
                googleMap = new google.maps.Map(document.getElementById('google__map'), mapOptions),
                mark_location = new google.maps.LatLng(latitude, longitude);

                googleMap.mapTypes.set('map_style', styledMap);
                googleMap.setMapTypeId('map_style');

                let marker = new google.maps.Marker({
                    position: mark_location,
                    map: googleMap,
                    icon: markerIcon
                });

                marker.setMap(googleMap);

                let infoWindow = new google.maps.InfoWindow({
                    content: address
                });

                google.maps.event.addListener(marker, 'click', function() {
                    infoWindow.open(googleMap, marker);
                });
        }
    }
}

let ntex = new GeneralClass();