<?php
/**
 *
 * @package WordPress
 * @subpackage N-tex
 * @since 1.0
 * @version 1.0
 */
?><!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <?php wp_head();  ?>
</head>
<body <?php body_class('load'); ?>>
    <?php wp_body_open(); 
    get_template_part( 'template-parts/header/preloader' ); ?>
    <header data-aos="fade-down" data-aos-duration="600">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <?php 
                    $logo = get_field('logo', 'option');
                    if( $logo ) { ?>
                    <a class="logo float-left" href="<?php echo esc_url( home_url( '/' ) ); ?>">
                        <img src="<?php echo $logo['url']; ?>" alt="<?php echo get_bloginfo('name'); ?>">
                    </a>
                    <?php } ?>
                    <div class="nav__bar float-right">
                        <?php if( has_nav_menu('main') ) { ?>
                        <div class="menu__block float-left">
                            <?php wp_nav_menu( array(
                                'theme_location'        => 'main',
                                'container'             => 'nav',
                                'container_class'       => 'main__nav'
                            ) ); ?>
                        </div>
                        <?php } 
                        $phone = get_field('phone', 'option'); 
                        $phone_2 = get_field('phone_2', 'option'); 
                        if( $phone || $phone_2 ) { ?>
                            <div class="phone__block float-left">
                                <?php if( $phone ) { ?>
                                    <a href="tel:<?php echo $phone; ?>" class="phone float-left"><?php echo $phone; ?></a>
                                <?php } 
                                if( $phone_2 ) { ?>
                                    <a href="tel:<?php echo $phone_2; ?>" class="phone float-left"><?php echo $phone_2; ?></a>
                                <?php } ?>
                            </div>
                        <?php } 
                        $btn_text = get_field('button_text', 'option'); 
                        if( $btn_text ) { ?>
                            <button class="btn btn__border"><?php echo $btn_text ?></button>
                        <?php } 
                        $languages = pll_the_languages(array('raw'=>1));
                        $current_lang = $lang_list = ''; 
                        foreach ( $languages as $lang ) { 
                            if( $lang['current_lang'] ){
                                $current_lang = $lang['name'];
                            } else {
                                $lang_list .= '<li><a href="'.$lang['url'].'">'.$lang['name'].'</a></li>';
                            }
                        } ?>
                        <div class="language__switcher">
                            <div class="current"><?php echo $current_lang; ?></div>
                            <ul>
                                <?php echo $lang_list; ?>
                            </ul>
                        </div> 
                    </div>
                    <div class="mobile__btn float-right">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <div class="mobile__menu">
        <?php if( has_nav_menu('main') ) { ?>
        <div class="menu__block float-left">
            <?php wp_nav_menu( array(
                'theme_location'        => 'main',
                'container'             => 'nav',
                'container_class'       => 'main__nav'
            ) ); ?>
        </div>
        <?php } 
        $phone = get_field('phone', 'option'); 
        $phone_2 = get_field('phone_2', 'option'); 
        if( $phone || $phone_2 ) { ?>
            <div class="phone__block float-left">
                <?php if( $phone ) { ?>
                    <a href="tel:<?php echo $phone; ?>" class="phone float-left"><?php echo $phone; ?></a>
                <?php } 
                if( $phone_2 ) { ?>
                    <a href="tel:<?php echo $phone_2; ?>" class="phone float-left"><?php echo $phone_2; ?></a>
                <?php } ?>
            </div>
        <?php } 
        $btn_text = get_field('button_text', 'option'); 
        if( $btn_text ) { ?>
            <div class="button__wrapper">
                <button class="btn btn__border"><?php echo $btn_text ?></button>
            </div>
        <?php } ?>
        <div class="language__switcher">
            <div class="current"><?php echo $current_lang; ?></div>
            <ul>
                <?php echo $lang_list; ?>
            </ul>
        </div> 
    </div>
    <main>