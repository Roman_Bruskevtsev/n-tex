<?php
/**
 *
 * @package WordPress
 * @subpackage N-tex
 * @since 1.0
 * @version 1.0
 */

get_header(); 

if( have_rows('content') ):

	while ( have_rows('content') ) : the_row();

		if( get_row_layout() == 'categories_section' ) :
			get_template_part( 'template-parts/page/content', 'categories_section' );
		elseif( get_row_layout() == 'clients_section' ) :
			get_template_part( 'template-parts/page/content', 'clients_section' );
		elseif( get_row_layout() == 'testimonials_section' ) :
			get_template_part( 'template-parts/page/content', 'testimonials_section' );
		endif;

	endwhile;

endif;

get_footer();