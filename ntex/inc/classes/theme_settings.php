<?php
/**
 * 
 */
class ThemeSettingsClass {
	const SCRIPTS_VERSION = '1.0.9';

	public function __construct(){
		$this->scriptsDir = get_theme_file_uri().'/assets/js';
        $this->stylesDir = get_theme_file_uri().'/assets/css';

		$this->actions_init();
	}

	public function actions_init(){
		add_action( 'wp_enqueue_scripts', array( $this, 'scripts_styles' ) );
		add_action( 'after_setup_theme', array( $this, 'theme_setup' ) );
		add_action( 'wp_footer',  array( $this, 'js_variables' ) );
		// add_action( 'widgets_init', array( $this, 'widgets_init') );
		add_action( 'init', array( $this, 'custom_posts_init' ) );
		add_action( 'init', array( $this, 'custom_taxonomy_init' ) );

		add_filter( 'upload_mimes', array( $this, 'enable_svg_types' ), 99 );
		add_filter( 'wpseo_json_ld_output', '__return_false' );
		add_filter( 'body_class', array( $this, 'body_class_names' ) );
	}

	public function scripts_styles() {
		wp_enqueue_style( 'ntex-css', $this->stylesDir.'/main.min.css' , '', self::SCRIPTS_VERSION);
    	wp_enqueue_style( 'ntex-style', get_stylesheet_uri() );

    	wp_enqueue_script( 'google-maps-key', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyA0-FJOs84Jsnf9pZgsR37XpxvBJG33xYo&libraries=places', array( 'jquery' ), self::SCRIPTS_VERSION, true );
    	wp_enqueue_script( 'all-js', $this->scriptsDir.'/all.min.js', array( 'jquery' ), self::SCRIPTS_VERSION, true );


    	// if( get_field('sharethis_api_key', 'option') && is_single() ) wp_enqueue_script( 'share-js', 'https://platform-api.sharethis.com/js/sharethis.js#property='.get_field('sharethis_api_key', 'option').'&product=custom-share-buttons&cms=sop', array( 'jquery' ), self::SCRIPTS_VERSION, true );
    }

    public function theme_setup(){
    	// if( ENV == 'dev' ) update_option( 'upload_url_path', 'https://stage.bcareagency.com/wp-content/uploads', true );

    	load_theme_textdomain( 'ntex' );
	    add_theme_support( 'automatic-feed-links' );
	    add_theme_support( 'title-tag' );
	    add_theme_support( 'post-thumbnails' );

	    // add_image_size( 'project-wide', 1170, 785, true );
	    // add_image_size( 'project-tall', 870, 1010, true );
	    // add_image_size( 'cases-image', 540, 304, true );
	    // add_image_size( 'post-thumbnail', 460, 460, true );
	    // add_image_size( 'post-thumbnail-wide', 940, 460, true );

	    register_nav_menus( array(
	        'main'          	=> __( 'Main Menu', 'ntex' ),
	        'footer'          	=> __( 'Footer Menu', 'ntex' )
	    ) );

	    if( function_exists('acf_add_options_page') ) {
		    $general = acf_add_options_page(array(
		        'page_title'    => __('Theme General Settings', 'ntex'),
		        'menu_title'    => __('Theme Settings', 'ntex'),
		        'redirect'      => false,
		        'capability'    => 'edit_posts',
		        'menu_slug'     => 'theme-settings',
		    ));
		}

		add_post_type_support( 'page', 'excerpt' );
    }

    public function widgets_init(){
    	register_sidebar( array(
	        'name'          => __( 'Footer 1 (contact information)', 'ntex' ),
	        'id'            => 'footer-1',
	        'description'   => __( 'Add widgets here to appear in your footer.', 'ntex' ),
	        'before_widget' => '<section id="%1$s" class="widget %2$s contact__widget">',
	        'after_widget'  => '</section>',
	        'before_title'  => '<h4>',
	        'after_title'   => '</h4>',
	    ) );
    }

    public function custom_posts_init(){
		$post_labels = array(
			'name'					=> __('Products', 'ntex'),
			'singular_name'			=> __('Product', 'ntex'),
			'add_new'				=> __('Add Product', 'ntex'),
			'add_new_item'			=> __('Add New Product', 'ntex'),
			'edit_item'				=> __('Edit Product', 'ntex'),
			'new_item'				=> __('New Product', 'ntex'),
			'view_item'				=> __('View Product', 'ntex')
		);

		$post_args = array(
			'label'               	=> __('Products', 'ntex'),
			'description'        	=> __('Product information page', 'ntex'),
			'labels'              	=> $post_labels,
			'supports'            	=> array( 'title'),
			'taxonomies'          	=> array( '' ),
			'hierarchical'       	=> false,
			'public'              	=> true,
			'show_ui'             	=> true,
			'show_in_menu'        	=> true,
			'has_archive'         	=> false,
			'can_export'          	=> true,
			'show_in_nav_menus'   	=> true,
			'publicly_queryable'  	=> false,
			'exclude_from_search' 	=> false,
			'query_var'           	=> true,
			'capability_type'     	=> 'post',
			'menu_position'			=> 3,
			'rewrite'				=> array(
				'slug'				=> 'products'
			),
			'menu_icon'           	=> 'dashicons-cart'
		);
		register_post_type( 'product', $post_args );
    }

    public function custom_taxonomy_init(){
		$taxonomy_labels = array(
			'name'                        => _x('Products categories', 'ntex'),
			'singular_name'               => _x('Product category', 'ntex'),
		);

		$taxonomy_rewrite = array(
			'slug'                  => 'products-categories',
			'with_front'            => true,
			'hierarchical'          => true,
		);

		$taxonomy_args = array(
			'labels'              => $taxonomy_labels,
			'hierarchical'        => true,
			'public'              => true,
			'show_ui'             => true,
			'show_admin_column'   => true,
			'show_in_nav_menus'   => true,
			'show_tagcloud'       => true,
			'rewrite'             => $taxonomy_rewrite,
		);
		register_taxonomy( 'products-categories', 'product', $taxonomy_args );

		$taxonomy_labels = array(
			'name'                        => _x('Products tags', 'ntex'),
			'singular_name'               => _x('Product tag', 'ntex'),
		);

		$taxonomy_rewrite = array(
			'slug'                  => 'products-tags',
			'with_front'            => true,
			'hierarchical'          => true,
		);

		$taxonomy_args = array(
			'labels'              => $taxonomy_labels,
			'hierarchical'        => false,
			'public'              => true,
			'show_ui'             => true,
			'show_admin_column'   => true,
			'show_in_nav_menus'   => true,
			'show_tagcloud'       => true,
			'rewrite'             => $taxonomy_rewrite,
		);
		register_taxonomy( 'products-tags', 'product', $taxonomy_args );
    }

    public function enable_svg_types($mimes) {
		$mimes['svg'] = 'image/svg+xml';
		return $mimes;
	}

	public function js_variables(){ ?>
		<script type="text/javascript">
	        var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
	    </script>
	<?php }

	public function google_tag_manager() { 
    	if ( ENV == 'prd' ) {
    		get_template_part( 'inc/analytics/gtm' );
    	}
    }

    public function schema_org() { 
    	if ( ENV == 'prd' ) {
    		get_template_part( 'inc/analytics/schema' );
    	}
    }

    public function google_tag_manager_noscript() { 
    	if ( ENV == 'prd' ) {
    		get_template_part( 'inc/analytics/gtm', 'noscript' );
    	}
    }

	public function body_class_names( $classes ) {
		if( is_page() && !is_front_page() ){
			$background = get_field('background_color');
			if( !$background ){
				$classes[] = 'grey__background';
			} else {
				$classes[] = 'white__background';
			}
		}
			
		return $classes;
	}

    public function __return_false() {
        return false;
    }
}

$theme_settings = new ThemeSettingsClass();