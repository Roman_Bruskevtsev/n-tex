<?php
/**
 *
 * @package WordPress
 * @subpackage N-tex
 * @since 1.0
 * @version 1.0
 */
get_header(); 
$current_term = get_queried_object();

$args = array(
	'tax_query'		=> array(
		array(
			'taxonomy'	=> 'products-categories',
			'field'		=> 'id',
			'terms'		=> $current_term->term_id
		)
	),
	'post_type'		=> 'product',
	'posts_per_page'=> -1

);
$query = new WP_Query( $args );
?>
	<section class="products__section">
		<div class="container">
			<div class="row">
				<div class="col-lg-6">
					<div class="title" data-aos="fade-right" data-aos-duration="600">
						<h1><?php echo $current_term->name; ?></h1>
					</div>
				</div>
				<?php if ( $query->have_posts() ) { ?>
				<div class="col-lg-6">
					<div class="tags float-right" data-aos="fade-left" data-aos-duration="600">
						<ul>
						<?php 
						$tags_array = [];
						while ( $query->have_posts() ) { $query->the_post(); 
							$tags = get_the_terms(get_the_ID(), 'products-tags'); 
							if( $tags ){
								foreach ( $tags as $tag ) { 
									if( !in_array($tag->term_id, $tags_array) ) $tags_array[] = $tag->term_id;
								}
							}
						} 
						if( $tags_array ){ 
							foreach ( $tags_array as $tag ) {
							$tag_object = get_term($tag, 'products-tags'); ?>
						 		<li data-id="<?php echo $tag_object->term_id; ?>"><?php echo $tag_object->name; ?></li>
							<?php } 
						}?>
						</ul>
					</div>
				</div>
				<?php } wp_reset_postdata(); ?>
			</div>
			<?php if ( $query->have_posts() ) { ?>
			<div class="row products__row">
				<?php while ( $query->have_posts() ) { $query->the_post(); 
					$tags = get_the_terms(get_the_ID(), 'products-tags');
					$tags_list = '';
					if( $tags_list ){
						foreach ($tags as $tag) {
						 	if( $tags_list == ''){
						 		$tags_list = $tag->term_id;
						 	} else {
						 		$tags_list = ' '.$tag->term_id;
						 	}
						} 
					}?>
				<div class="col-lg-6 col-xl-3 product__cell <?php echo $tags_list; ?>">
					<div class="product">
						<a class="thumbnail" href="<?php echo get_field('image')['url']; ?>">
							<?php if( get_field('image') ) { ?>
							<img src="<?php echo get_field('image')['url']; ?>" alt="<?php the_title(); ?>">
							<?php } ?>
						</a>
						<button class="btn btn__green"><?php _e('Order', 'ntex'); ?></button>
						<div class="text">
							<h3><?php the_title(); ?></h3>
							<?php the_field('description'); ?>
						</div>
					</div>
				</div>
				<?php } ?>
			</div>
			<?php } wp_reset_postdata(); 
			$description = get_term($current_term->term_id, 'products-categories'); 
			if( $description->description ){ ?>
			<div class="row">
				<div class="col">
					<div class="term__description" data-aos="fade-up" data-aos-duration="600"><p><?php echo $description->description; ?></p></div>
				</div>
			</div>
			<?php }	?>
		</div>
		
	</section>
<?php get_footer();