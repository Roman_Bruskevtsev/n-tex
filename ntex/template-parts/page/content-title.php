<section class="page__title" data-aos="fade-up" data-aos-duration="600">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<h1><?php the_title(); ?></h1>
				<?php the_field('subtitle'); ?>
			</div>
		</div>
	</div>
</section>