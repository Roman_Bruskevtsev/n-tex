<?php $gallery = get_sub_field('gallery'); 
if( $gallery ) { ?>
<section class="gallery__section">
	<div class="container">
		<div class="row">
		<?php foreach ( $gallery as $image ) { 
			$background = ' style="background-image: url('.$image['url'].')"'; ?>
			<div class="col-sm-6 col-lg-3">
				<a href="<?php echo $image['url']; ?>" class="image"<?php echo $background; ?> data-aos="fade-up" data-aos-duration="600"></a>
			</div>
		<?php } ?>
		</div>
	</div>
</section>
<?php } ?>