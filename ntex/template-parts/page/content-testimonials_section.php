<?php
$feedbacks = get_sub_field('testimonials');
if( $feedbacks ) { ?>
<section class="testimonials__section">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-lg-8">
				<div class="feedback__slider">
				<?php foreach ( $feedbacks as $feedback ) { ?>
					<div class="slide">
						<div class="content text-center">
							<?php echo $feedback['text']; ?>
							<div class="author">
								<h3><?php echo $feedback['name']; ?></h3>
								<span class="position"><?php echo $feedback['position']; ?></span>
							</div>
						</div>
					</div>
				<?php } ?>
				</div>
			</div>
		</div>
	</div>
</section>
<?php } ?>