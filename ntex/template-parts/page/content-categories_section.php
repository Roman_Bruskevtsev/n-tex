<section class="categories__section">
	<div class="container">
		<?php if( get_sub_field('title') ) { ?>
		<div class="row">
			<div class="col">
				<div class="title text-center" data-aos="fade-up" data-aos-duration="600">
					<h2><?php echo get_sub_field('title'); ?></h2>
				</div>
			</div>
		</div>
		<?php } 
		$categories = get_sub_field('choose_categories_to_show'); 
		if( $categories ) { ?>
		<div class="row">
			<?php foreach ( $categories as $category ) { 
				$thumbnail = get_field('thumbnail', 'products-categories_'.$category->term_id);
				$triangle = get_field('triangle', 'products-categories_'.$category->term_id);
				$border = get_field('border-color', 'products-categories_'.$category->term_id) ? ' style="border-color: '.get_field('border-color', 'products-categories_'.$category->term_id).'"' : '';
			?>
			<div class="col-md-6 col-lg-4">
				<a class="category" href="<?php echo get_term_link($category->term_id, 'products-categories'); ?>" data-aos="fade-up" data-aos-duration="600">
					<div class="thumbnail">
						<?php if( $triangle ) { ?>
						<div class="triangle">
							<img src="<?php echo $triangle['url']; ?>" alt="<?php echo $category->name; ?>">
						</div>
						<?php } ?>
						<?php if( $thumbnail ) { ?>
						<div class="image">
							<img src="<?php echo $thumbnail['url']; ?>" alt="<?php echo $category->name; ?>">
						</div>
						<?php } ?>
					</div>
					<div class="title text-center"<?php echo $border; ?>>
						<h3><?php echo $category->name; ?></h3>
					</div>
				</a>
			</div>
			<?php } ?>
		</div>
		<?php } ?>
	</div>
</section>