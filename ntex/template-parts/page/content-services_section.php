<?php 
$services = get_sub_field('services');
if( $services ) { ?>
<section class="services__section">
	<div class="container">
		<div class="row">
		<?php foreach ( $services as $service ) { ?>
			<div class="col-lg-6 col-xl-3">
				<div class="service" data-aos="fade-left" data-aos-duration="600">
					<div class="image">
						<?php 
						$image = $service['image'];
						if( $image ) { 
							$background = $image ? ' style="background-image: url('.$image['url'].');"' : ''; ?>
							<div class="icon"<?php echo $background; ?>></div>
						<?php } ?>
						<?php if( $service['title'] ) { ?><h3><?php echo $service['title']; ?></h3><?php } ?>
					</div>
					<?php if( $service['short_text'] ) { ?>
					<div class="text short__text"><?php echo $service['short_text']; ?></div>
					<?php } ?>
					<?php if( $service['full_text'] ) { ?>
					<div class="text full__text"><?php echo $service['full_text']; ?></div>
					<?php } ?>
					<div class="show__more"><?php _e('Read more', 'ntex'); ?></div>
					<div class="show__less"><?php _e('Hide', 'ntex'); ?></div>
				</div>
			</div>
		<?php } ?>
		</div>
	</div>
</section>
<?php } ?>