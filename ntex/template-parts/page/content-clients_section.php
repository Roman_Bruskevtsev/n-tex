<?php
$clients = get_sub_field('clients');
if( $clients ) { ?>
<section class="clients__section">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="clients__slider">
				<?php foreach ( $clients as $client ) { ?>
					<div class="client">
						<img src="<?php echo $client['url']; ?>" alt="<?php echo $client['title']; ?>">
					</div>
				<?php } ?>
				</div>
			</div>
		</div>
	</div>
</section>
<?php } ?>