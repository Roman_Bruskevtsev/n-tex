<?php 
$form = get_field('order_form', 'option');
if( $form['form_shortcode'] ) { ?>
<div class="popup order">
	<span class="close"></span>
	<div class="form">
		<?php if( $form['title'] ) { ?><h3><?php echo $form['title']; ?></h3><?php } ?>
		<?php echo $form['text']; ?>
		<?php echo do_shortcode( $form['form_shortcode'] ); ?>
	</div>
</div>
<?php } ?>