<?php
/**
 * Template Name: Contacts
 *
 * @package WordPress
 * @subpackage N-tex
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>
	<section class="contact__section">
		<div class="container">
			<div class="row">
				<div class="col-lg-6">
					<div class="content" data-aos="fade-up" data-aos-duration="600">
						<h1><?php the_title(); ?></h1>
						<?php the_field('subtitle'); ?>
						<div class="contact__block">
						<?php if( get_field('address') ) { ?>
							<a href="https://www.google.com/search?q=<?php the_field('address'); ?>" class="address"><?php the_field('address'); ?></a>
						<?php } 
						if( get_field('email') ) { ?>
							<a href="mailto:<?php the_field('email'); ?>" class="email"><?php the_field('email'); ?></a>
						<?php }
						if( get_field('phone') ) { ?>
							<a href="tel:<?php the_field('phone'); ?>" class="phone"><?php the_field('phone'); ?></a>
						<?php } ?>
						</div>
						<?php if( get_field('form_shortcode') ) echo do_shortcode( get_field('form_shortcode') ); ?>
					</div>
				</div>
			</div>
		</div>
		<div class="container-fluid">
			<div class="row justify-content-end">
				
				<div class="col-lg-6">
					<?php 
					$map = get_field('map');
					?>
					<div id="google__map" class="map" data-str="<?php the_field('address'); ?>" data-latitude="<?php echo $map['latitude']; ?>" data-longitude="<?php echo $map['longitude']; ?>" data-zoom="<?php echo $map['map_zoom']; ?>" data-marker="<?php echo $map['marker']; ?>"></div>
				</div>
			</div>
		</div>
	</section>
<?php get_footer();