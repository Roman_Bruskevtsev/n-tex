<?php
/**
 *
 * @package WordPress
 * @subpackage N-tex
 * @since 1.0
 * @version 1.0
 */
?>
    </main>
    <footer>
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-3">
					<?php 
                    $logo = get_field('footer_logo', 'option');
                    if( $logo ) { ?>
                    <a class="logo float-left" href="<?php echo esc_url( home_url( '/' ) ); ?>">
                        <img src="<?php echo $logo['url']; ?>" alt="<?php echo get_bloginfo('name'); ?>">
                    </a>
                    <?php } ?>
                    <div class="copyright">© <?php echo date('Y'); ?> N-tex</div>
				</div>
				<div class="col-lg-6">
					<?php if( has_nav_menu('main') ) { ?>
	                <div class="menu__block float-left">
	                    <?php wp_nav_menu( array(
	                        'theme_location'        => 'main',
	                        'container'             => 'nav',
	                        'container_class'       => 'footer__nav text-center'
	                    ) ); ?>
	                </div>
	                <?php } ?>
				</div>
				<div class="col-lg-3">
					<div class="social__block float-right">
						<ul>
							<?php if( get_field('facebook', 'option') ) { ?>
							<li>
								<a href="<?php echo get_field('facebook', 'option'); ?>" class="fb" target="_blank"></a>
							</li>
							<?php } 
							if( get_field('instagram', 'option') ) { ?>
							<li>
								<a href="<?php echo get_field('instagram', 'option'); ?>" class="in" target="_blank"></a>
							</li>
							<?php } ?>
						</ul>
						<a href="https://fajta.com.ua/" target="_blank" class="prod text-right">© Marketing Studio FAJTA</a>
					</div>
				</div>
			</div>
		</div>
    </footer>
    <div class="call__btn"><div class="text"><?php _e('Order a call', 'ntex'); ?></div></div>
    <div class="popup__wrapper"></div>
    <?php 
    get_template_part( 'template-parts/footer/popup', 'order' );
    get_template_part( 'template-parts/footer/popup', 'contact' );
    wp_footer(); ?>
</body>
</html>